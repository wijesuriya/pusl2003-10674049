import { Component, OnInit } from '@angular/core';
import { NewService } from '../news.service';
import {ne} from '../news';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.css']
})
export class NewsComponent implements OnInit {

  
  news:ne[];
  constructor(public lol: NewService) { }

 

  ngOnInit(): void {
    this.lol.getnews().subscribe(news=>{
      console.log(news);
      this.news=news;
    });
  }
}