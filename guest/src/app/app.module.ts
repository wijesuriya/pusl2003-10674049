import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import {AgmCoreModule} from '@agm/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { FacultiesComponent } from './faculties/faculties.component';
import { RouterModule } from '@angular/router';
import { NotfoundComponent } from './notfound/notfound.component';
import { AboutComponent } from './about/about.component';
import { DiscoverComponent } from './discover/discover.component';
import { InternationalComponent } from './international/international.component';
import { HomeComponent } from './home/home.component';
import { NewsComponent } from './news/news.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';


import { HeaderComponent, ModalContact } from './header/header.component';
import { BusinessComponent } from './business/business.component';
import { ComputingComponent } from './computing/computing.component';
import { EngineringComponent } from './enginering/enginering.component';
import { ScheduleModule, RecurrenceEditorModule, AgendaService ,DayService, MonthService, WeekService, WorkWeekService, MonthAgendaService} from '@syncfusion/ej2-angular-schedule';
import { LifeAtNSBMComponent } from './life-at-nsbm/life-at-nsbm.component';
import { CalenderComponent } from './calender/calender.component';
import { LocationComponent } from './location/location.component';
import { SportsComponent } from './sports/sports.component';
import { ActivityComponent } from './activity/activity.component';
import { InternationalActivityComponent } from './international-activity/international-activity.component';
import { AcademicComponent } from './academic/academic.component';
import { ReligiousComponent } from './religious/religious.component';
import {AngularFireModule} from 'angularfire2';
import {AngularFireDatabaseModule, AngularFireDatabase} from 'angularfire2/database';
import {AngularFirestoreModule} from '@angular/fire/firestore';
import { environment } from 'src/environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FacultiesComponent,
    NotfoundComponent,
    AboutComponent,
    DiscoverComponent,
    InternationalComponent,
    HomeComponent,
    NewsComponent,
    ModalContact,
    BusinessComponent,
    ComputingComponent,
    EngineringComponent,
    LifeAtNSBMComponent,
    CalenderComponent,
    LocationComponent,
    SportsComponent,
    ActivityComponent,
    InternationalActivityComponent,
    AcademicComponent,
    ReligiousComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule,
    ScheduleModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFirestoreModule,
    RecurrenceEditorModule,
    AgmCoreModule.forRoot({
      apiKey:'AIzaSyDkmWSn9IRMLI-J-Kx8bsqkTWzfbRJqfAI'
    })
  ],
  providers: [AgendaService ,DayService, MonthService, WeekService, WorkWeekService, MonthAgendaService],
  bootstrap: [AppComponent]
})
export class AppModule { }
