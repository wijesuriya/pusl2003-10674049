import { Injectable } from '@angular/core';
import { Observable} from 'rxjs';
import {AngularFirestore, AngularFirestoreCollection, AngularFirestoreDocument} from '@angular/fire/firestore';
import {ne} from '../app/news';

@Injectable({
  providedIn: 'root'
})
export class NewService {

  nesdata: AngularFirestoreCollection<ne>;
  newl: Observable<ne[]>;

  constructor(private db:AngularFirestore) { 
    this.newl = this.db.collection('news').valueChanges();
  }
 
  getnews(){
    return this.newl;
  }
}

