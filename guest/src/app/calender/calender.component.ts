import { Component, OnInit } from '@angular/core';
import { EventSettingsModel,View } from '@syncfusion/ej2-angular-schedule';

@Component({
  selector: 'app-calender',
  templateUrl: './calender.component.html',
  styleUrls: ['./calender.component.css']
})
export class CalenderComponent implements OnInit {

  constructor() { }

  public setview: View="Month";
  public setDate: Date = new Date(2020,2,15)

  public eventobject: EventSettingsModel ={
    dataSource:[{
      Subject:"Sports Meet",
      StartTime: new Date(2020,2,27,4,0),
      EndTime: new Date(2020,2,27,6,0),
      IsAllDay: true,
      IsReadonly: true
    }]
  }

  ngOnInit(): void {
  }

}
