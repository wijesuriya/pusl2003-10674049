import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {AppComponent} from './app.component';
import {FacultiesComponent} from './faculties/faculties.component';
import {AboutComponent} from './about/about.component';
import {NotfoundComponent} from './notfound/notfound.component';
import { HomeComponent } from './home/home.component';
import { DiscoverComponent } from './discover/discover.component';
import { InternationalComponent } from './international/international.component';
import { NewsComponent } from './news/news.component';
import { BusinessComponent } from './business/business.component';
import { ComputingComponent } from './computing/computing.component';
import { EngineringComponent } from './enginering/enginering.component';
import { LifeAtNSBMComponent } from './life-at-nsbm/life-at-nsbm.component';
import { CalenderComponent } from './calender/calender.component';
import { LocationComponent } from './location/location.component';
import { SportsComponent } from './sports/sports.component';
import { ActivityComponent } from './activity/activity.component';
import { InternationalActivityComponent } from './international-activity/international-activity.component';
import { ReligiousComponent } from './religious/religious.component';
import { AcademicComponent } from './academic/academic.component';


const routes: Routes = [  
{path:'', component: HomeComponent},
{path:'faculties', component: FacultiesComponent},
{path: 'about us' , component: AboutComponent},
{path: 'location' , component: LocationComponent},
{path: 'Sports clubs' , component: SportsComponent},
{path: 'Academic clubs' , component: AcademicComponent},
{path: 'Activity clubs' , component: ActivityComponent},
{path: 'International clubs' , component: InternationalActivityComponent},
{path: 'Religious clubs' , component: ReligiousComponent},
{path: 'Discover NSBM' , component: DiscoverComponent},
{path: 'international' , component: InternationalComponent},
{path: 'business' , component: BusinessComponent},
{path: 'computing' , component: ComputingComponent},
{path: 'enginering' , component: EngineringComponent},
{path: 'news' , component: NewsComponent},
{path: 'life at NSBM' , component: LifeAtNSBMComponent},
{path: 'calender' , component: CalenderComponent},
{path: '**', component: NotfoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
