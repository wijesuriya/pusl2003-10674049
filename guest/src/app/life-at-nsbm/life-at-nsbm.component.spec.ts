import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LifeAtNSBMComponent } from './life-at-nsbm.component';

describe('LifeAtNSBMComponent', () => {
  let component: LifeAtNSBMComponent;
  let fixture: ComponentFixture<LifeAtNSBMComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LifeAtNSBMComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LifeAtNSBMComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
