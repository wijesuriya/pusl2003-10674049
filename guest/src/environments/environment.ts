// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebase:{
    apiKey: "AIzaSyDrV8Etrzo1Bu-ciArKp5KdZEbCWtAyXV8",
    authDomain: "fir-demo-c11dc.firebaseapp.com",
    databaseURL: "https://fir-demo-c11dc.firebaseio.com",
    projectId: "fir-demo-c11dc",
    storageBucket: "fir-demo-c11dc.appspot.com",
    messagingSenderId: "436971786185",
    appId: "1:436971786185:web:094202507a9f046037e218",
    measurementId: "G-9MHH7NL7VX"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
